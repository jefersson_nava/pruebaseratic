package jefersson.nunez.boleteria

/**
 * The User entity.
 *
 * @author  Jefersson Nava
 * @version 1.0.0
 * @since
 *
 */
class User {
	static searchable = true
	
    String username
    String passwordHash
	
	String nombre
	String apellido
    String telefono
	String fechaRegistro
	String habilitado = "S"
	
    static hasMany = [ roles: Role ]

    static constraints = {
        username(nullable: false, blank: false, unique: true)
		passwordHash nullable:false, blank:false
		nombre nullable: false, blank: false, size:1..100
		apellido nullable: true, blank: true, size:0..100
		habilitado nullable:false, blank:false, size:0..1, inList:["S","N"]
		fechaRegistro nullable:true, blank:true, size:0..1024
		telefono nullable:true, blank:true, size:1..20
    }
	
	static mapping = {
		version false
		table 'USUARIO'
		id generator:'native'
		columns{
			id column:'ID'
			username column:'usuario'
			passwordHash column:'clave'
			nombre column:'nombre'
			apellido column:'apellido'
			telefono column:'telefono'
			fechaRegistro column:'fechaRegistro'
			habilitado column:'habilitado'
		}
	}
}
