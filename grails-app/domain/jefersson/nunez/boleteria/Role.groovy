package jefersson.nunez.boleteria

/**
 * The Role entity.
 *
 * @author  Jefersson Nava
 * @version 1.0.0
 * @since
 *
 */
class Role {
	static searchable = true
    String name

    static hasMany = [ users: User, permissions: String ]
    static belongsTo = User

    static constraints = {
        name(nullable: false, blank: false, unique: true)
    }
	
	static mapping = {
		version false
		table 'ROLE'
		id generator:'native'
		columns{
			id column:'ID'
			name column:'nombre'
		}
	}
}
