package jefersson.nunez.boleteria

class Boleto {
	static searchable = true
	Double valor
	Evento evento
	Tipo tipo
	
    static constraints = {
		
    }
	
	static mapping = {
		version false
		table 'BOLETO'
		id generator:'native'
		columns{
			id column:'ID'
			valor column:'valor'
			evento column:'evento'
		}
	}
}
