package jefersson.nunez.boleteria

class Cliente {
	static searchable = true
	Long cedula
	String nombre
	String telefono
	String correo

    static constraints = {
		
    }
	
	static mapping = {
		version false
		table 'CLIENTE'
		id generator:'native'
		columns{
			id column:'id'
			cedula column:'cedula'
			nombre column:'nombre'
			correo column:'correo'
			telefono column:'telefono'
		}
	}
}
