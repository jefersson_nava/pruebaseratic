package jefersson.nunez.boleteria

class Reserva {
	static searchable = true
	Cliente cliente
	Boleto boleto
	Date fecha
	Long cantidadBoletos
	
    static constraints = {
    }
	
	static mapping = {
		version false
		table 'RESERVA'
		id generator:'native'
		columns{
			id column:'id'
			cliente column:'cliente'
			boleto column:'boleto'
			fecha column:'fecha'
			cantidadBoletos column:'cantidadBoletos'
		}
	}
}
