package jefersson.nunez.boleteria

class Tipo {
	static searchable = true
	String nombre

    static constraints = {
    }
	static mapping = {
		version false
		table 'TIPO'
		id generator:'native'
		columns{
			id column:'ID'
			nombre column:'valor'
		}
	}
}
