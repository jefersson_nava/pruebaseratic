package jefersson.nunez.boleteria

class Evento {
	static searchable = true
	String nombre
	String direccion
	String ciudad
	Date fecha
	Long cantidadBoletos

    static constraints = {
		nombre(nullable: false, blank: false)
		direccion(nullable: false, blank: false)
		ciudad(nullable: false, blank: false)
		fecha(nullable: false, blank: false)
		cantidadBoletos(nullable: false, blank: false)
    }
	static mapping = {
		version false
		table 'EVENTO'
		id generator:'native'
		columns{
			id column:'id'
			nombre column:'nombre'
			direccion column:'direccion'
			ciudad column:'ciudad'
			fecha column:'fecha'
			cantidadBoletos column:'cantidadBoletos'
		}
	}
}

