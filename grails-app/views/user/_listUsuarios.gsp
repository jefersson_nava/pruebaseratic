	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
	</g:if>
	<g:if test="${userInstanceList.empty}">
		<p id="noEntries"><g:message code="default.noEntries.label"/></p>
	</g:if>
	<g:else>
		<div class="table-responsive"> 
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th class="mail-box check-mail"><input type="checkbox" value="" name="" class="i-checks" disabled/></th>
						<th class="text-center"><g:message code="user.name.label" /></th>
						<th class="text-center"><g:message code="user.lastName.label" /></th>
						<th class="text-center"><g:message code="user.telefono.label" /></th>
						<th class="text-center"><g:message code="user.username.label" /></th>
						<th class="text-center"><g:message code="user.roles.label" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${userInstanceList}" status="i" var="userInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'} fil" data-id="${userInstance.id }">
						
						<td>
							<input type="checkbox" value="" name="ckeckbox-usuarios" class="i-checks" data-id="${userInstance.id }" ${userInstance.username == 'superAdmin' ? 'disabled' : ''}/>
	                                      					 
						</td>
						<td class="text-center">
							<g:link action="show" id="${userInstance?.id}">${fieldValue(bean: userInstance, field: "nombre")}
							</g:link>
						</td>
						<td class="text-center">${fieldValue(bean: userInstance, field: "apellido")}</td>
						<td class="text-center">${fieldValue(bean: userInstance, field: "telefono")}</td>
						<td class="text-center">${fieldValue(bean: userInstance, field: "username")}</td>
						
						<td class="text-center">
							<g:each in="${userInstance?.roles}" status="x" var="role">
								${role.name },
							</g:each>
						</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
		</div>
		<div class="pagination mi-paginacion-ajax">
			<g:paginate total="${userInstanceCount ?: 0}" params="${params}"/>
		</div>
	</g:else>