<%@ page import="jefersson.nunez.boleteria.User" %>



<div class="hidden">
  <input type="password"/>
</div>
      
<input type="hidden" name="query" id="params-query" value="${params.query }" />

<div class="form-group ${hasErrors(bean: userInstance, field: 'nombre', 'error')} required">
	<label for="name" class="col-sm-4 control-label">
		<g:message code="user.name.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8">
		<g:textField name="nombre" class="input-sm col-xs-8 col-sm-8" required="" value="${userInstance?.nombre}"/>
	</div>
</div>

<div class="form-group ${hasErrors(bean: userInstance, field: 'apellido', 'error')} required">
	<label for="lastName" class="col-sm-4 control-label">
		<g:message code="user.lastName.label" default="Apellido" />
	</label>
	<div class="col-sm-8">
		<g:textField name="apellido" class="input-sm col-xs-8 col-sm-8" value="${userInstance?.apellido}"/>
	</div>
</div>

<div class="form-group ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
	<label for="username" class="col-sm-4 control-label">
		<g:message code="user.username.label" default="Usuario" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8">
		<g:textField name="username" class="input-sm col-xs-8 col-sm-8" required="" value="${userInstance?.username}"/>
	</div>
</div>

<div class="div-content-password">
	<div class="form-group ${hasErrors(bean: userInstance, field: 'passwordHash', 'error')} required">
		<label for="passwordHash" class="col-sm-4 control-label">
			<g:message code="user.passwordHash.label" default="Contraseña" />
			<span class="required-indicator">*</span>
		</label>
		<div class="col-sm-8">
			<input name="passwordHash" type="password" class="input-sm col-xs-8 col-sm-8" readonly onfocus="this.removeAttribute('readonly');" value="${userInstance?.passwordHash}"/>
		</div>
	</div>
</div>

<div class="form-group ${hasErrors(bean: userInstance, field: 'telefono', 'error')} required">
	<label for="eMail" class="col-sm-4 control-label">
		<g:message code="user.telefono.label" default="Teléfono" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8">
		<input type="text" name="telefono" class="input-sm col-xs-8 col-sm-8" required="required" autocomplete="off" value="${userInstance?.telefono}">
	</div>
</div>




<div class="form-group ${hasErrors(bean: userInstance, field: 'roles', 'error')} ">
	<label for="roles" class="col-sm-4 control-label">
		<g:message code="user.roles.label" default="Perfiles" />
	</label>
	<div class="col-sm-8">
		<g:select name="roles" from="${roleInstanceList}" multiple="true" data-placeholder="Ninguno" optionKey="id" optionValue="name" value="${userInstance?.roles*.id}" class="chosen-select"/>
	</div>
</div>

