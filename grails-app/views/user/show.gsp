
<%@ page import="jefersson.nunez.boleteria.User" %>
<!doctype html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'user.label', default: 'Usuario')}" />
<title><g:message code="user.title.label" /></title>
<asset:stylesheet src="plugins/sweetalert/sweetalert.css" />
</head>
<body>

	<div class="col-lg-12">
	<g:hiddenField id="urlBase" name="urlBase" value="${createLink(uri:'/')}" />
	    <div class="ibox float-e-margins">
	        <div class="ibox-title">
	            <h3>
					<g:message code="user.header.show.label" />
				</h3>
	            <div class="ibox-tools">
	            </div>
	        </div>
	        <div class="ibox-content ibox-heading">
	           <g:if test="${flash.message}">
					<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						${flash.message}
					</div>
				</g:if>
				<div class="container-fluid">
					<div class="col-md-6 col-sm-6">
						<dl>
							<g:if test="${userInstance?.nombre}">
								<dt>
									<g:message code="user.name.label" default="Nombre" />
								</dt>
		
								<dd>
									<g:fieldValue bean="${userInstance}" field="nombre" />
								</dd>
		
							</g:if>
						</dl>
					</div>
					<div class="col-md-6 col-sm-6">
						<dl>
							<g:if test="${userInstance?.apellido}">
								<dt>
									<g:message code="user.lastName.label" default="lastName" />
								</dt>
		
								<dd>
									<g:fieldValue bean="${userInstance}" field="apellido" />
								</dd>
		
							</g:if>
						</dl>
					</div>
					<div class="col-md-6 col-sm-6">
						<dl>
							<g:if test="${userInstance?.username}">
								<dt>
									<g:message code="user.username.label" default="Nombre" />
								</dt>
		
								<dd>
									<g:fieldValue bean="${userInstance}" field="username" />
								</dd>
		
							</g:if>
						</dl>
					</div>
					<div class="col-md-6 col-sm-6">
						<dl>
							<g:if test="${userInstance?.telefono}">
								<dt>
									<g:message code="user.telefono.label" default="Email" />
								</dt>
		
								<dd>
									<g:fieldValue bean="${userInstance}" field="telefono" />
								</dd>
		
							</g:if>
						</dl>
					</div>
					
				</div>
	        </div>
	        <div class="ibox-content inspinia-timeline">
	
	            <div class="timeline-item">
	                <div class="row">
	                    
	                    
	                    
	                </div>
	            </div>
	           
	           
	            
	
	        </div>
	    </div>
	</div>
	
</body>
</html>

