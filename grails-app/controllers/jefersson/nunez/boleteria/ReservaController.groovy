package jefersson.nunez.boleteria



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.converters.JSON


class ReservaController {
	def searchableService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Reserva.list(params), model:[reservaInstanceCount: Reserva.count()]
    }

	def listBoleto(){
		//params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def res = [:]
		def total = 0
		def list = [:]
		
		if (!params.query?.trim()) {
			list = Boleto.list(params)
			total = Boleto.count()
		}else{
			def searchResults = Boleto.search(params.query, params)
			total = searchResults.total
			list = searchResults.results
		}
		
		render (list as JSON)
	}
	
    def create() {
		def res = [:]
        //Tipo de boleto
		//Evento 
		def eventoInstance = Evento.get(params.evento as Long)
		if (!eventoInstance) {
			res['status'] = "error"
			res['messagge'] = "No se encontr� el Evento"
			render (res as JSON)
			return
		}
		//Buscar cliente por cedula
		def clienteInstance = Cliente.findByCedula(params.cedula as Long)
		if(!clienteInstance){
			//Crear cliente
			clienteInstance = new Cliente()
			clienteInstance.cedula = params.cedula as Long
			clienteInstance.nombre = params.nombre
			clienteInstance.telefono = params.telefono
			clienteInstance.correo = params.correo
			if (!clienteInstance.save(flush: true)) {
				println clienteInstance.errors
				res['status'] = "error"
				res['messagge'] = "No se pudo crear el Cliente"
				render (res as JSON)
				return
			}
		}
		def tipo = Tipo.get(params.tipo as Long)
		
		//Buscar Boleto
		def boletoInstance = Boleto.findByTipo(tipo)
		if (!boletoInstance) {
			res['status'] = "error"
			res['messagge'] = "No se encontr� el Tipo de Boleto"
			render (res as JSON)
			return
		}
		
		//Buscar si hay boletos disponibles
		def reservas = Reserva.findAllByBoleto(boletoInstance)
		def boletosReservados = 0
		reservas.each{reser->
			boletosReservados += reservas?.cantidadBoletos
		}
		def boletosEvento = 0
		boletosEvento = eventoInstance?.cantidadBoletos
		if(!(boletosEvento > boletosReservados)){
			res['status'] = "error"
			res['messagge'] = "No hay boletos disponibles"
			render (res as JSON)
			return
		}
		
		def bole =  (params.cantidadBoletos as Integer) + boletosReservados
		if(bole>boletosEvento){
			res['status'] = "error"
			res['messagge'] = "No hay boletos suficientes intente menos boletos"
			render (res as JSON)
			return
		}
		
		def reservaInstance = new Reserva()
		reservaInstance.cliente = clienteInstance
		reservaInstance.boleto = boletoInstance
		reservaInstance.cantidadBoletos = params.cantidadBoletos as Integer
		reservaInstance.fecha = new Date()
		if (!reservaInstance.save(flush: true)) {
			res['status'] = "error"
			res['messagge'] = "No se pudo crear la reserva"
			render (res as JSON)
			return
		}
		
		res['status'] = "success"
		res['messagge'] = "Reserva creada con exito"
		render (res as JSON)
    }

    def list(){
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def res = [:]
		def total = 0
		def list = [:]
		
		if (!params.query?.trim()) {
			list = Reserva.list(params)
			total = Reserva.count()
			render (list as JSON)
			return
		}else{
			def c = Reserva.createCriteria()
			def results = c.list {
			    if(params.cedula){
					def cliente = Cliente.findByCedula(params.cedula as Long)
					eq("cliente",cliente)
				}
				if(params.evento){
					def evento = Evento.findByNombre(params.nombre)
					eq("evento",evento)
				}
			}
			
			list = results
			total = results.size()
			render (list as JSON)
			return
		}
		
		res['status'] = "error"
		res['messagge'] = "No hay par�metro de b�squeda"
		render (res as JSON)
	}

}
