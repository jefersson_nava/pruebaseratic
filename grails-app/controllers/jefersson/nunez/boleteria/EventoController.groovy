package jefersson.nunez.boleteria



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException


class EventoController {
	def searchableService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
        params.max = Math.min(max ?: 10, 100)
        respond Evento.list(params), model:[eventoInstanceCount: Evento.count()]
    }

	def list(){
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def eventoInstanceList = Evento.list(params)
		render eventoInstanceList as JSON
	}

    def create() {
		def res = [:]
		def fecha = new Date().parse("dd/MM/yyyy hh:mm",params.fechac)
		def userInstance = new Evento(params)
		userInstance.fecha = fecha
		
		if (!userInstance.save(flush: true)) {
			res['status'] = "error"
			res['messagge'] = "No se pudo crear el Evento"
			render (res as JSON)
			return
		}
		
		res['status'] = "success"
		res['message'] = "Se cre� exitosamente el evento"
		render (res as JSON)
		return
    }
	
	def search(){
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def res = [:]
		def total = 0
		def list = [:]
		
		if (!params.query?.trim()) {
			
		}else{
			def searchResults = Evento.search(params.query, params)
			total = searchResults.total
			list = searchResults.results
			render (list as JSON)
			return
		}
		
		res['status'] = "error"
		res['messagge'] = "No hay par�metro de b�squeda"
		render (res as JSON)
	}

    def edit() {
		def res = [:]
        def userInstance = Evento.get(params.id)
		if (!userInstance) {
			res['status'] = "error"
			res['messagge'] = "No se encontr� el Evento"
			render (res as JSON)
			return
		}
		
		userInstance.properties = params
		
		if (!userInstance.save(flush: true)) {
			res['status'] = "error"
			res['messagge'] = "No se pudo editar el Evento"
			render (res as JSON)
			return
		}
		
		res['status'] = "success"
		res['message'] = "Se edit� exitosamente el evento"
		render (res as JSON)
    }
	
	def delete(){
		def res = [:]
		def userInstance = Evento.get(params.id)
		if (!userInstance) {
			res['status'] = "error"
			res['messagge'] = "No se encontr� el Evento"
			render (res as JSON)
			return
		}
		
		try {
			userInstance.delete(flush: true)
			res['status'] = "success"
			res['message'] = "Se elimin� exitosamente el evento"
			render (res as JSON)
			return
		}
		catch (DataIntegrityViolationException e) {
			res['status'] = "error"
			res['messagge'] = "No se pudo eliminar el Evento"
			render (res as JSON)
			return
		}
	}

}
