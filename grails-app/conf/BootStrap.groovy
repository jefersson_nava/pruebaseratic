import jefersson.nunez.boleteria.Role
import jefersson.nunez.boleteria.User
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def init = { servletContext ->
		def superAdminRole= new Role(name: 'Super Administrador')
		superAdminRole.addToPermissions('*:*')
		superAdminRole.save()
		
		def user = new User(username: "admin", passwordHash: new Sha256Hash("admin").toHex(),nombre:"Jefersson",apellido:"Nava",
			fechaRegistro:"25/09/2017",telefono:"3217222934",habilitado:"S")
		user.addToRoles(superAdminRole)
		user.save()
		println user.errors
		
    }
    def destroy = {
    }
}
